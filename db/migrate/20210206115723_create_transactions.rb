class CreateTransactions < ActiveRecord::Migration[6.1]
  def change
    create_table :transactions do |t|
      t.text :description
      t.integer :trans_type, limit: 1, default:0
      t.decimal :balance, :precision => 15, :scale => 2, null: false,default: 0
      t.decimal :running_balance, :precision => 15, :scale => 2, null: false,default: 0
      t.timestamps
    end
  end
end
